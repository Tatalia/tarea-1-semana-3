/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2;

import java.util.Scanner;

public class Tarea2 {

    public static void main(String[] args) {
        //Supermarket(args);
        //Coffee(args);
        Chairs(args);

    }

    public static void Supermarket(String[] args) {
        Scanner valueFromKeyboard = new Scanner(System.in);
        Scanner stringFromKeyboard = new Scanner(System.in);
        int variableClient = 0;

        int rice = 1000;
        int riceQuantity = 0;

        int cocacola = 1500;
        int cocacolaQuantity = 0;

        int bread = 800;
        int breadQuantity = 0;

        double totalAmount = 0;

        String username = "";
        int userAge = 0;
        int clientType = 1; //1 = Unitario   //2 = Mayorista
        String clientTypeString = "";

        while (variableClient <= 5) {

            System.out.println("\nDigite su Nombre: ");
            username = stringFromKeyboard.nextLine();
            System.out.println("Digite su Edad: ");
            userAge = valueFromKeyboard.nextInt();
            System.out.println("Digite 1 si es cliente Unitario y 2 si es Mayorista: ");
            clientType = valueFromKeyboard.nextInt();
            while (clientType != 1 && clientType != 2) {
                System.out.println("El numero insertado es incorrecto. Digítelo nuevamente.");
                clientType = valueFromKeyboard.nextInt();
            }
            //parte b
            //parte c
            System.out.println("Cuantas unidades desea de arroz? ");
            riceQuantity = valueFromKeyboard.nextInt();
            System.out.println("Cuantas unidades desea de Coca Cola? ");
            cocacolaQuantity = valueFromKeyboard.nextInt();
            System.out.println("Cuantas unidades desea de pan?");
            breadQuantity = valueFromKeyboard.nextInt();
            //parte c
            //parte d
            totalAmount = (rice * riceQuantity) + (cocacola * cocacolaQuantity) + (bread * breadQuantity);
            if (clientType == 2) {
                totalAmount = totalAmount - (totalAmount * 0.15);
            }
            //parte d
            //parte e
            if (clientType == 1 && userAge >= 25 && userAge <= 35) {
                totalAmount = totalAmount - (totalAmount * 0.05);
            }
            //parte e
            if (clientType == 1) {
                clientTypeString = "Unitario";
            } else {
                clientTypeString = "Mayorista";
            }
            //parte f
            System.out.println(
                    "Nombre: " + username + "\n"
                    + "Edad: " + userAge + "\n"
                    + "Tipo de Cliente: " + clientTypeString + "\n"
                    + "Monto a pagar: " + totalAmount
            );
            //parte f
            variableClient = +1;

        }
    }

    public static void Coffee(String[] args) {
        Scanner valueFromKeyboard = new Scanner(System.in);

        int variableFriend = 1;
        int noSugar = 0;
        int milk = 0;
        int sweet = 0;
        int salty = 0;

        while (variableFriend <= 3) {

            System.out.println("\nSi desea el café con azúcar, digite 1. De lo contrario, digite 2: ");
            int sugar = valueFromKeyboard.nextInt();

            while (sugar != 1 && sugar != 2) {
                System.out.println("Número inválido, intente nuevamente. ");
                sugar = valueFromKeyboard.nextInt();
            }

            if (sugar == 2) {
                noSugar += 1;
            }

            System.out.println("Si desea el café con leche, digite 1. De lo contrario, digite 2: ");
            int askMilk = valueFromKeyboard.nextInt();

            while (askMilk != 1 && askMilk != 2) {
                System.out.println("Número inválido, intente nuevamente. ");
                askMilk = valueFromKeyboard.nextInt();
            }

            if (askMilk == 1) {
                milk += 1;
            }

            System.out.println("Si prefiere pan dulce, digite 1. Si lo prefiere salado, digite 2: ");
            int bread = valueFromKeyboard.nextInt();

            while (bread != 1 && bread != 2) {
                System.out.println("Número inválido, intente nuevamente. ");
                bread = valueFromKeyboard.nextInt();
            }
            if (bread == 1) {
                sweet += 1;
            } else {
                salty += 1;
            }

            System.out.println("\nPersonas que desean tomar café sin azúcar: " + noSugar
                    + "\nPersonas que desean tomar café con leche: " + milk
                    + "\nPersonas que prefieren pan dulce: " + sweet
                    + "\nPersonas que prefieren pan salado: " + salty);

            variableFriend += 1;
        }

    }
    
    public static void Chairs(String[] args){
        Scanner valueFromKeyboard = new Scanner(System.in);
        Scanner stringFromKeyboard = new Scanner(System.in);

        int amountChair = 0;
        double totalAmount = 0;
        String colorChair = "";
        
        System.out.println("Digite la cantidad de sillas a comprar: ");
        amountChair = valueFromKeyboard.nextInt();
        
        System.out.println("¿Desea las sillas de color? (Sí o No)");
        colorChair = stringFromKeyboard.nextLine();
        
        if(amountChair > 8){
            totalAmount = amountChair * 10000;
        }else if(amountChair <= 8 || amountChair >= 4){
            totalAmount = amountChair * 11000;
        }else{
            totalAmount = amountChair * 15000;
        }
        
        if(colorChair.equalsIgnoreCase("Si")){
            totalAmount = (totalAmount * 0.05) + totalAmount;
        }
        
        System.out.println("El monto total a pagar por " + amountChair + " silla(s) es de: " + totalAmount);
    } 

}
